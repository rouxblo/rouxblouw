---

title: "String calculator"
date: 2022-06-01T21:29:01+08:00
lastmod: 2022-06-01T21:29:01+08:00
draft: false
author: "Robert Louw"
description: "This code kata is used to practice test-driven development, typing speed and familiarity with your 
chosen programming language and IDE."

images: ['beach-kata-kick.jpg']
resources:
- name: "featured-image"
  src: "beach-kata-kick.jpg"

tags: ["code-kata", "tdd", "beginner"]

---

<!--more-->

> Originally taken from [osherove.com](http://osherove.com/tdd-kata-1/) and reformatted to my preference.

## The goal

This code kata is used to practice test-driven development, typing speed and familiarity with your chosen programming language and IDE.
Complete this kata using strict test-driven development. Start by writing a unit test, making it fail followed 
by writing the minimum code to make the test pass. The aim is to do the entire solution following this approach within 30 minutes.
Don't worry, you will not get it right the first few times. You are meant to practice it over and over again, just like a real kata
until parts of it become muscle memory.

## Before you start
* Do not read ahead.
* Do one requirement at a time. The trick is to learn to work incrementally.
* You only have to test for correct inputs for the requirements. There is no need to test for invalid inputs.

## Requirements

1. Create a simple method/function with the name `add` that takes a `string` as an input and returns an `int`.
2. The method should return 0 when an empty string is provided. Example input of `""`.
3. The method should return the number as an integer if only a number is provided. Example input of `1`.
4. The method should return the sum of two comma-separated numbers. Example input of `1,2` should return `3`.
5. The method should return the sum of any amount of comma-separated numbers. Example input of `1,2,3` should return `6`.
6. Allow the add method to also handle a newline character (`\n`) as a separator. Example input of `1\n2,3` should return `6`.
7. Add support for custom delimiters specified with the input in the format of `//[delimiter]\n[numbers...]`. For example, `//;\n1;2` should return `3` where
the delimiter is `;`. All previous scenarios should still be supported.
8. Providing a negative number within the input must throw an exception with the message `negatives not allowed: [negatives...]`. All of the negatives provided in the input should be appended to the exception message. For example, an input of `//;\n1;-2;3;-4` should produce an exception with an error message `negatives not allowed: [-2, -4]`.

### Now for things to get a bit more challenging...

9. Numbers larger than 1000 should be ignored in the final sum. For example `1,1001,2` should return 3.
10. Custom delimiters can now be of any length in the format of `//[delimiter]\n`. For example `//[***]\n1***2***3` should return `6`.
11. Allow for multiple custom delimiters in the format of `//[delim1][delim2]\n`. For example `//[*][%]\n1*2%3` should return `6`.
12. Ensure that your method can also handle multiple custom delimiters of more than one character. For example `/[***][%%%]\n1***2%%%3` should return `6`.

## References
* https://deangerber.com/kata/string-calculator/
* http://osherove.com/tdd-kata-1/