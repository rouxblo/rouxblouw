---
title: "Bookmarks"
date: 2022-05-24T19:00:00+02:00
draft: false

images: ["/images/library.jpg"]

---


![Banner](images/library.jpg "Algorithmic problems, tests and challenges")

Below you can find bookmarks of algorithmic problems, tests and challenges that I've posted. I've put them here as I tend to reference these often.

## Code katas
> An exercise in programming which helps software developers improve their skills through practice and repetition.[^codekata]

### [String Calculator](/string-calculator)
  I've done this one so many times that I've lost count. I've found it to be one of the most effective ways to introduce developers to test-driven development.
  It can also be quite useful to practice the basics of a new programming language or get more comfortable with your
  keyboard and IDE.

[^codekata]: A quick code kata description from here: <https://apiumhub.com/tech-blog-barcelona/code-kata>