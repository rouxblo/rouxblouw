# Recommended as a build image on https://gohugo.io/getting-started/installing/
FROM klakegg/hugo:0.93.2-busybox as build

WORKDIR /usr/app
COPY . .
RUN hugo

# FROM registry.gitlab.com/rouxblouw/images/spa as runtime

FROM nginx:1.22.0-alpine as runtime

# Custom config
COPY nginx.conf /etc/nginx/nginx.conf

# Current dir is where the static files should go
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*

# Add limited permissions for nginx user
RUN chown -R nginx:nginx /var/cache/nginx && \
    chown -R nginx:nginx /var/log/nginx && \
    chown -R nginx:nginx /etc/nginx/conf.d

USER nginx

COPY --from=build /usr/app/public .
